#!/bin/bash

cd "/c/Users/yanik/OneDrive - Siemens AG/Projekte"
find . -maxdepth 1 -mindepth 1 -type d -print -execdir git --git-dir={}/.git --work-tree="$PWD"/{} pull \;

cd "/c/Users/yanik/OneDrive - Siemens AG/Personal Projects"
find . -maxdepth 1 -mindepth 1 -type d -print -execdir git --git-dir={}/.git --work-tree="$PWD"/{} pull \;